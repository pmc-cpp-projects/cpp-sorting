#include <stdio.h>
#include <iostream.h>
#include <stdlib.h>

class MyProj
{
public:
	void sorting();
	void ugly_numbers();
	void character_counter();
};


// ==================== SORTING ====================
void MyProj::sorting(void)
{
	int num[32], elements, temp, ctr, ctr2, ctr3, ctr4, index=0;
	char pressed, pressed2, pressed3;

	do
	{
	system("cls");
	cout << "\t\t\tSORTING\n\n";
	
	// ===================== INITIALIZE ELEMENTS
	do
	{
	cout << "Enter no. of element to be sorted [3-15]? "; cin >> elements;
	}
	while((elements < 3) || (elements > 15));
	
	cout << endl;

	for(ctr=0; ctr < elements; ctr++)
	{
		cout << "Enter " << ctr + 1<< " number: "; cin >> num[ctr];
	}

	// ==================== FIRST MENU
	cout << endl << endl;

	cout << "[A] Selection\n";
	cout << "[B] Insertion\n";
	cout << endl;
	do
	{
	cout << "Enter Your Choice: "; cin >> pressed2;
	}
	while((toupper(pressed2) != 'A') && (toupper(pressed2) != 'B'));

	// ==================== SECOND MENU
	cout << endl << endl;

	cout << "[A] Ascending\n";
	cout << "[B] Descending\n";
	cout << endl;
	do
	{
	cout << "Enter Your Choice: "; cin >> pressed3;
	}
	while((toupper(pressed3) != 'A') && (toupper(pressed3) != 'B'));

	// ==================== SHOW THE ORIGINAL
	for(ctr=0; ctr < elements; ctr++) cout << num[ctr] << " ";
	cout << "\t  " << " Original\n\n";

	if (toupper(pressed2) == 'A')
	{
		// ==================== ASCENDING SELECTION SORT ====================
		if (toupper(pressed3) == 'A')
		{
			cout << "\t\t\tASCENDING SELECTION SORT\n\n";	

			// ==================== LOOP FOR THE PIVOT
			for(ctr=0; ctr < elements; ctr++)
			{
				index=ctr;
	
				// ==================== LOOP FOR CHECK THE LOWEST VALUE
				for(ctr2=ctr+1; ctr2 < elements; ctr2++) if (num[index] > num[ctr2]) index = ctr2;

				// ==================== SWAP
				if(num[ctr] > num[index])
				{
					temp = num[ctr];
					num[ctr] = num[index];
					num[index] = temp;
				}
	
				// ==================== SHOW
				for(ctr2=0; ctr2 < elements; ctr2++) cout << num[ctr2] << " ";
	
				cout << "\t " << ctr + 1 << " Pass\n";
			}
		}
		// ==================== DESCENDING SELECTION SORT ====================
		else
		{
			cout << "\t\t\tDESCENDING SELECTION SORT\n\n";	
	
			// ==================== LOOP FOR THE PIVOT
			for(ctr=elements-1, ctr4=0; ctr > -1; ctr--, ctr4++)
			{
				index=ctr;
		
				// ==================== LOOP FOR CHECK THE LOWEST VALUE
				for(ctr2=ctr-1; ctr2 > -1; ctr2--) if (num[index] > num[ctr2]) index = ctr2;
	
				// ==================== SWAP
				if(num[ctr] > num[index])
				{
					temp = num[ctr];
					num[ctr] = num[index];
					num[index] = temp;
				}
	
				// ==================== SHOW
				for(ctr2=0; ctr2 < elements; ctr2++) cout << num[ctr2] << " ";
	
				cout << "\t " << ctr4 + 1 << " Pass\n";
			}
	
		}
	}
	else
	{
		// ==================== ASCENDING INSERTION SORT ====================
		if (toupper(pressed3) == 'A')
		{

			cout << "\t\t\tASCENDING INSERTION SORT\n\n";

			// ==================== LOOP FOR TO BE SORTED
			for(ctr3=2; ctr3 <= elements; ctr3++)
			{
				// ==================== LOOP FOR THE PIVOT
				for(ctr=0; ctr < ctr3 ; ctr++)
				{
					// ==================== LOOP FOR SECOND PIVOT
					for(ctr2=ctr+1; ctr2 < ctr3; ctr2++)
					{
						// ==================== SWAP
						if(num[ctr] > num[ctr2])
						{
							temp = num[ctr];
							num[ctr] = num[ctr2];
							num[ctr2] = temp;
						}
					}
				}
		
				// ==================== SHOW
				for(ctr2=0; ctr2 < elements; ctr2++) cout << num[ctr2] << " ";
		
				cout << "\t " << ctr3 - 1 << " Pass\n";
			}
		}
		// ==================== DESCENDING INSERTION SORT ====================
		else
		{
			
			cout << "\t\t\tDESCENDING INSERTION SORT\n\n";

			// ==================== LOOP FOR TO BE SORTED
			for(ctr3=elements-2, ctr4=0; ctr3 >= 0; ctr3--, ctr4++)
			{
				// ==================== LOOP FOR THE PIVOT
				for(ctr=elements-1; ctr >= ctr3; ctr--)
				{
					// ==================== LOOP FOR SECOND PIVOT
					for(ctr2=ctr-1; ctr2 >= ctr3; ctr2--)
					{
						// ==================== SWAP
						if(num[ctr] > num[ctr2])
						{
							temp = num[ctr];
							num[ctr] = num[ctr2];
							num[ctr2] = temp;
						}
					}
				}

				// ==================== SHOW
				for(ctr2=0; ctr2 < elements; ctr2++) cout << num[ctr2] << " ";
	
				cout << "\t " << ctr4 + 1 << " Pass\n";
			}
		}
	}
		
	cout << endl << endl;
	
	do
	{
	cout << "Try Again[Y/N]? "; cin >> pressed;
	}
	while((toupper(pressed) != 'Y') && (toupper(pressed) != 'N'));
	}
	while(toupper(pressed) != 'N');
}


// ==================== UGLY NUMBERS ====================
void MyProj::ugly_numbers(void)
{

	int position, pointer=0, remaining;
	char pressed;

	do
	{
		system("cls");
		cout << "\t\t\tUGLY NUMBERS\n\n";

		cout << "Ugly Numbers are nos. whose prime factors are 2, 3, or 5.\n";
		cout << "1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15 are the first 11 Ugly Numbers.\n";
		cout << "By convention, 1 is included.\n\n\n";
	
		cout << "Enter nth position: "; cin >> position;
		cout << endl;

		cout << "Ugly Numbers\n\n";
	
		for(int ctr=1; ctr <= position; ctr++)
		{
			cout << ctr << "\t Position is \t";
			do
			{
				pointer++;
				remaining=pointer;

				// ==================== LOOP FOR CHECK IF UGLY NUMBER
				while((remaining != 1) && (
					((remaining % 2) == 0) ||
					((remaining % 3) == 0) ||
					((remaining % 5) == 0)))
	
				{
					if (remaining % 2 == 0) remaining /= 2;
					else if (remaining % 3 == 0) remaining /=3;
					else if (remaining % 5 == 0) remaining /=5;
				}
			
			}
			while(remaining != 1);
			cout << pointer << endl;
		}

		cout << endl << endl;

		do
		{
		cout << "Try Again[Y/N]? "; cin >> pressed;
		}
		while((toupper(pressed) != 'Y') && (toupper(pressed) != 'N'));
	
	}
	while(toupper(pressed) != 'N');
	cout << endl;
}


// ==================== CHARACTER COUNTER ====================
void MyProj::character_counter(void)
{
int ctr, ctr2;
char pressed;
char string[256];
int num[53] =
     {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
char character[53] =
     {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
      'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',' '};

do
{	
	for(ctr=0; ctr < 53; ctr++) num[ctr]=0;
	system("cls");
	printf("\t\t\tCHARACTER COUNTER\n\n");      

	printf("Enter a string: "); gets(string);

	// ==================== TALLY CHARACTERS
	for(ctr=0; ctr < 53; ctr++) for(ctr2=0; ctr2 < 53; ctr2++) if(string[ctr] == character[ctr2]) num[ctr2]++;
	
	cout << "\n\t\t\tCounted Characters\n\n";
	
	// ==================== SHOW TALLYED CHARACTERS
	for(ctr=0; ctr < 26; ctr++)
	{
		cout << "\t\t" << character[ctr+26] << " : " << num[ctr+26] << "\t\t\t" << character[ctr] << " : " << num[ctr] << endl;
	}

	cout << "\t\t" << "(Space)" << " : " << num[52];
	cout << endl << endl;

	do
	{
		cout << "Try Again[Y/N]? "; cin >> pressed;
	}
	while((toupper(pressed) != 'Y') && (toupper(pressed) != 'N'));

}
while(toupper(pressed) != 'N');
}

// ==================== MAIN MENU ====================
void main()
{
	MyProj p;
	char pressed;

	do
	{
	system("cls");
	cout << "\t\t\tSORTING\n\n";

	cout << "[A] Sorting\n";
	cout << "[B] Ugly Numbers\n";
	cout << "[C] Character Counter\n";
	cout << "[X] Exit\n";
	cout << endl;
	do
	{
	cout << "Enter Your Choice: "; cin >> pressed;
	}
	while((toupper(pressed) != 'A') &&
		(toupper(pressed) != 'B') &&
		(toupper(pressed) != 'C') &&
		(toupper(pressed) != 'X'));
	
	switch(toupper(pressed))
	{
		case 'A' : p.sorting(); break;
		case 'B' : p.ugly_numbers(); break;
		case 'C' : p.character_counter(); break;
	}

	}
	while(toupper(pressed) != 'X');
}
